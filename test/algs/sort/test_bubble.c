#include "test_bubble.h"

void test_bubble_sort(void)
{
    int list[] = {8, 6, 7, 5, 3, 0, 9};

    printf("Starting Bubble Sort test case...\n");

    printf("Before swap:\n");
    present(list, 7);

    bubble_sort(list, 7);

    printf("After swap:\n");
    present(list, 7);

    printf("Bubble Sort test case complete.\n");
    
    return;
}