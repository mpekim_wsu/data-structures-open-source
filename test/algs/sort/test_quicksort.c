#include "test_quicksort.h"

void test_quicksort(void)
{
    int list[] = {8, 6, 7, 5, 3, 0, 9};

    printf("Starting Quicksort test case...\n");

    printf("Before swap:\n");
    present(list, 7);

    quicksort(list, 0, 7);

    printf("After swap:\n");
    present(list, 7);

    printf("Quicksort test case complete.\n");

    return;
}