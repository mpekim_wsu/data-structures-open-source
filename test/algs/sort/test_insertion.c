#include "test_insertion.h"

void test_insertion_sort(void)
{
    int list[] = {8, 6, 7, 5, 3, 0, 9};

    printf("Starting Insertion Sort test case...\n");

    printf("Before swap:\n");
    present(list, 7);

    insertion_sort(list, 7);

    printf("After swap:\n");
    present(list, 7);

    printf("Insertion Sort test case complete.\n");
    
    return;
}