#include "test_selection.h"

void test_selection_sort(void)
{
    int list[] = {8, 6, 7, 5, 3, 0, 9};

    printf("Starting Selection Sort test case...\n");

    printf("Before swap:\n");
    present(list, 7);

    selection_sort(list, 7);

    printf("After swap:\n");
    present(list, 7);

    printf("Selection Sort test case complete.\n");
    
    return;
}