#include "test_stack.h"

void test_stack(void)
{
    Stack* s1 = create_stack(24);

    push(s1, 17);
    push(s1, 66);

    pop(s1);
    
    view_top(s1);

    printf("Is the stack empty? %d\n", stack_is_empty(s1));
    printf("%d\n", pop(s1));
    printf("Is the stack empty? %d\n", stack_is_empty(s1));

    destroy_stack(&s1);

    return;
    // Driver for Stack.
}