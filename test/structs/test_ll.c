#include "test_ll.h"

void test_linkedlist(void)
{
    LinkedList* l1 = create_linkedlist_default();
    
    present_list(l1);

    printf("Adding nodes...\n");

    add_node_end(l1, 24);
    add_node_end(l1, 26);

    present_list(l1);

    printf("Removing nodes...\n");
    //remove_node(l1, 24);
    //remove_node(l1, 26);
    // Figure out why removing nodes causes issues.
    // May have to do with improper implementation.

    present_list(l1);

    destroy_linkedlist(&l1);
    return;
    // Driver for LinkedList.
}