#include "test_bt.h"

void test_binary_tree(void)
{
    BinaryTree* bt1 = create_binary_tree_default();
    
    add_leaf_left(bt1, 2);
    add_leaf_right(bt1, 1);

    printf("Presenting Pre-Order:\n");
    present_preorder(bt1->root);
    printf("\n");

    printf("Presenting In-Order:\n");
    present_inorder(bt1->root);
    printf("\n");

    printf("Presenting Post-Order:\n");
    present_postorder(bt1->root);
    printf("\n");
    
    destroy_binary_tree(&bt1);
    // Driver for Binary Tree. All functions assume input.
    // Please create tests for user-generated input soon.
    return;
}