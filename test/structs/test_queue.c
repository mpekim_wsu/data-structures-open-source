#include "test_queue.h"

void test_queue(void)
{
    Queue* q1 = create_queue_default();

    present_queue(q1);
    printf("\n");

    enqueue(q1, 1);
    enqueue(q1, 2);

    present_queue(q1);
    printf("\n");

    dequeue(q1);
    
    present_queue(q1);
    printf("\n");

    destroy_queue(&q1);

    return;
    // Driver for Queue.
}