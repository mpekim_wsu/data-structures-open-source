#include <stdio.h>
#include <stdlib.h>
#include "./structs/linkedlist.h"
#include "./structs/binary_tree.h"
#include "./test/structs/test_bt.h"

int main(int argc, char* argv[])
{
    // Driver for algorithm.
    /*int list[7] = {8, 6, 7, 5, 3, 0, 9};

    printf("Presenting list:\n");
    present(list, 7);

    insertion_sort(list, 7);
    bubble_sort(list, 7);
    selection_sort(list, 7);

    printf("Presenting list:\n");
    present(list, 7);*/
    
    //test_queue();
    //test_stack();
    test_binary_tree();

    //test_linear_search();

    return 0;
}