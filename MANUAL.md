# DATA STRUCTURES (OPEN SOURCE)
##### *By Mike Morley*
------------------------------------------
------------------------------------------
# TABLE OF CONTENTS

1. [Foreword](#foreword)
2. [How to Use](#how-to-use)
 - Compiling Programs
 - Test Cases
3. [Data Structures](#data-structures)
 - Node
 - Leaf
 - LinkedList
 - Stack 
 - Queue
 - Binary Tree
4. [Sorting Algorithms](#sorting-algorithms)
- Insertion Sort
- Bubble Sort
- Selection Sort
5. [Searching Algorithms](#searching-algorithms)
- Linear Search
- Binary Search

6. [References](#references)


------------------------------------------
------------------------------------------
# FOREWORD

Welcome to the Data Structures Open Source repository!
This is a collection of various data structures and algorithms (mainly written with the C language) for anyone to use and modify.

These data structures have been provided to allow programmers
access to their appearance, as well as an understanding
as to how they work within an IDE such as Visual Studio Code.

**# Note 1:** Code should be tested regularly to ensure
performance. Bugs or other errors should be reported and
fixed as soon as possible.
**# Note 2:** This code may be used as a reference for various classes and
assignments. However, credit ***must*** be given whenever possible to the
original author (*Mike Morley*).

*This code may NOT be copied/pasted, or otherwise used to cheat or plagiarize*
*on assignments. This note relieves the original author of any reponsibilities,*
*penalties or other negative factors of unintended use of the code.*

------------------------------------------
------------------------------------------
# HOW TO USE

- All code should be run through the `main.c` file. 
This is the driver/testing section of this repository.
- Two folders hold the data structures and algorithms contained within the
project. Add the desired structures and algorithms to `main.c`.
- **Pre-made test cases coming soon!**
- When ready, compile and run the code.

## COMPILING PROGRAMS

***Note:*** Compilation has been done using Visual Studio 2019.
However, the steps have been written as to be used with any
acceptable IDE.

## TEST CASES

------------------------------------------
------------------------------------------
# DATA STRUCTURES

***Note:*** at this time, all data structures are only designed to work with 
`int` data. 
- Other data types may be implemented in future updates.
- Data conversions (such as type casting) have also been considered.

------------------------------------------
------------------------------------------
## NODE

- The `Node` is the **basic building block** of the various
data structures within this repository.

*All of the other data structures within this manual can be assembled with LinkedLists, which are assembled with Nodes!*
*The only exception to this is the Tree data structures, which involve Leaves.*

A Node consists of two elements:
- `Data` - can consist of anything (variable types, classes, etc.)
- `Node*` - pointer to the next node.

```plantuml
hide circle
skinparam ClassAttributeIconSize 0
hide empty attributes
hide empty methods

class **Node**{
    -data: int
    -next: Node*
}
```

------------------------------------------
------------------------------------------
## LEAF
A `leaf` is a special type of Node.

It is pretty much the same in structure and behavior
to a Node - but the leaf will have different pointers
to other leaves. 
In the sense of a `Binary Tree`, leaves
will have two pointers: one to the *left* leaf, and one
to the *right* leaf.

```plantuml
hide circle
skinparam ClassAttributeIconSize 0
hide empty attributes
hide empty methods
class **Leaf**{
    -data: int
    -left: Leaf*
    -right: Leaf*
}
```
------------------------------------------
------------------------------------------
## LINKEDLIST

```plantuml
hide circle
skinparam classAttributeIconSize 0
hide empty attributes
hide empty methods

class Node1{
    - data: int
    - next: Node*
}

note left
First node is called **Head**
end note

class Node2{
    - data: int
    - next: Node*
}

class Node3{
    - data: int
    - next: Node*
}

class Node4{
    - data: int
    - next: Node*
}

Node1 --right--> Node2
Node2 --right--> Node3
Node3 --right--> Node4
```

- A `LinkedList` is a data structure that consists of Nodes. In the most typical/basic type of `LinkedList`, these Nodes are lined up one after another, with only one direction of travel.
- As mentioned within the intro, almost every other type of Data Structure in this repository can be constructed
using a style of `LinkedList` (single, double, circular, tree, etc.)

------------------------------------------
------------------------------------------
## STACK

```plantuml
hide circle
skinparam ClassAttributeIconSize 0
hide empty attributes
hide empty methods

class Node1{
    - data: int
    - next: Node*
}

class Node2{
    - data: int
    - next: Node*
}

class Node3{
    - data: int
    - next: Node*
}

class Node4{
    - data: int
    - next: Node*
}

Node1 -down-> Node2
Node2 -down-> Node3
Node3 -down-> Node4

note left of Node1
Only the top Node (the top of 
the Stack) can be viewed.
end note
```
- A `Stack` can be seen as a "unique" type of LinkedList.
- Stacks run on the LIFO (Last in, First Out) principle.
This essentially means that data is accessed in an order that
is reversed compared to how the data was put in.
- Another important feature about the Stack, is that only the
"top" element (the most recently added element) can be viewed
or accessed at any time.
***Hint: Think of this like a stack of cards, where all other***
***cards are covered by the topmost one.***
- In order to access other elements within the Stack, they need
to become the "new top" by "Popping" (removing) the current
top element.

**Featured Functions**
```c
push(int value)
```
The `push()` function takes an integer value, and pushes it onto the stack.
This is done by creating a new node with the integer, and then making that
node the head of the new stack.

```c
int pop(void)
```

The `pop()` function takes the top-most value (the one most recently added
to the Stack) and removes it. This value is then returned to be used by the
program.

```c
int top(void)
```

The `top()` function runs similarly to the `pop()` function. The main difference
is that the `top()` function merely shows the top value, while the `pop()` function
also removes it from the Stack.

```c
Bool stack_is_empty(Stack* list)
```

The `stack_is_empty()` will take a Stack, and check to see if there is any values within
the Stack. If so, then the Stack will return `FALSE`. If there are no values
within the Stack (meaning the Stack is empty), then `TRUE` will be returned.

------------------------------------------
------------------------------------------
## QUEUE

```plantuml
hide circle
hide empty attributes
hide empty methods

class Node1{

}

class Node2{

}

class Node3{

}

class Node4{

}

Node1 --right--> Node2
Node2 --right--> Node3
Node3 --right--> Node4
```

- A `Queue` is a data structure that is similar to a `Stack`.
- However, the Queue uses the FIFO (First In, First Out)
policy.

- A special type of `Queue`, called the `Priority Queue`, takes
an additional argument: the priority value.
- With this priority value, certain items can move to the front/
back of a sorted list despite not technically being "in order";
this is due to the priority arrangement coming before the data
value arrangement.

- **NOTE:** A Queue can be emulated through two stacks. That is:
*a first stack can be used to put elements in a "reversed order",
and then the second stack can "reverse the order" again (putting
the elements in "proper" FIFO order).*

**Featured Functions**
```c
void enqueue(Queue* list, int value)
```
The `enqueue` function will take an item and add it to the queue.
It requires two parameters: a pointer to a Queue object, and an
int variable for the value to be added.
***Note:*** The item added to the queue will ALWAYS be put in the
back, since queues follow the FIFO (First In, First Out) policy.

```c
void dequeue(Queue* list)
```

The `dequeue` function will take the first member of the Queue,
and remove it. This function does not need a value to reference,
since Queues work similar to real-life lines (meaning that the
first item added to the Queue will also be the first one removed).

------------------------------------------
------------------------------------------
## BINARY TREE

```plantuml
hide circle
hide empty attributes
hide empty methods

class **Leaf**{
    -data: int
    -left: Leaf*
    -right: Leaf*
}

note left
Leaf has two pointers
end note

class Leaf1{

}

class Leaf2{

}

class Leaf3{

}

class Leaf4{
    
}

class Leaf5{
    
}

class Leaf6{
    
}

class Leaf7{
    
}


Leaf1 --down--> Leaf2
Leaf1 --down--> Leaf3
Leaf2 --down--> Leaf4
Leaf2 --down--> Leaf5
Leaf3 --down--> Leaf6
Leaf3 --down--> Leaf7

```

- The `Binary Tree` is a special type of tree data structure.
While general trees can have all sorts of branching patterns,
each node in a Binary Tree has two connections (branches), a right
and a left branch.
- The Binary Tree structure is often set up on a "skew" system.
Examples include "left-skewed", where leaf nodes are occupying the
left branch before the right one; "right-skewed" is another example,
using leaf nodes on the right branch before the left one.
- **NOTE:** *Binary Trees may contain a "key" in order to access certain
nodes instantaneously.*

------------------------------------------
------------------------------------------

# SORTING ALGORITHMS
------------------------------------------
------------------------------------------
## INSERTION SORT

`Insertion Sort` is a sorting algorithm that is often used
with the data structures provided in this repository.

*Algorithm Process:*
- Start with a "single" unit of the list.
- As the we traverse through the list, it "grows in size"
(each element read is added to the list).
- If an element is larger/smaller than the one next to it,
then the two elements are swapped.

***Time Complexity***
Best: O(n)
Worst: O(n^2)
Average: O(n^2)

------------------------------------------
------------------------------------------
## BUBBLE SORT

`Bubble Sort` is one of the simplest algorithms used
for sorting collections of data.

*Algorithm Process:*
- Take a list of data.
- Using two iterators, compare two elements of data.
- If one element is larger/smaller than the other,
have the two elements swap positions.
- Run through the process until the list has been
sorted.

***Time Complexity*** 
*Best:* `O(n)`
*Worst:* `O(n^2)`
*Average:* `O(n^2)`

------------------------------------------
------------------------------------------
## SELECTION SORT

`Selection Sort` is another sorting method used to order data.

*Algorithm Process*
- Take a list of data.
- Set the first element to either min/max value
(depending on whether or not the list is Ascending/
Descending).
- Compare this "max/min" to other values in the list.
- If the new value is larger/smaller, make it the new
extrema.
- When the list is exhausted, the final extreme value will
be sorted in the *n*th position (based on the iteration of
the loop).
- Repeat the process until the list has been sorted.

***Time Complexity***
*Best:* `O(n^2)`
*Worst:* `O(n^2)`
*Average:* `O(n^2)`

------------------------------------------
------------------------------------------
## QUICKSORT

***Time Complexity***
*Best:* `O(n log(n))`
*Worst:* `O(n^2)`
*Average:* `O(n log(n))`

# SEARCHING ALGORITHMS

------------------------------------------
------------------------------------------
## LINEAR SEARCH

Linear Search is the most basic searching algorithm.
While easy to understand and implement, it is very
ineffective.

*Algorithm Process*
- Take a list of data. (While sorting would optimize
the process, it does not need to be sorted).
- Check each value and compare it to the desired
value. If found, return the desired item.
- If the list is exhausted and the item is not found,
then it does not exist in the list.

***Time Complexity***
*Best*: `O(1)`
*Worst*: `O(n)`
*Average*: `O(n)`
------------------------------------------
------------------------------------------
## BINARY SEARCH

Binary Search is a much more effective searching algorithm
than Linear Search. It is also relatively easy to understand
and implement.

*Algorithm Process*
- Take a list of sorted data.
- Find and set the min/max values for the list.
- Check each of these values with the desired value.
- If found, return the desired item. Otherwise,
divide the list into two sections.
- Check whether the desired item is lower or higher
than the current guess, and search within the half
that matches the required direction (lower/higher).

***Time Complexity***
*Best:* `O(1)`
*Worst:* `O(log n)`
*Average:* `O(log n)`

------------------------------------------
------------------------------------------
## EXPONENTIAL SEARCH

***Time Complexity***
*Best:* `O()`
*Worst:* `O()`
*Average:* `O()`

------------------------------------------
------------------------------------------
 # REFERENCES

 Unless otherwise noted, all algorithm and data structure
 implementations have been referenced, assisted or used from
 the coding website "Geeks For Geeks".