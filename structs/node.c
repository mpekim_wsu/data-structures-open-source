#include "node.h"

Node* create_node_default(void)
{
    Node* new_node;
    new_node = (Node*)malloc(sizeof(Node));
    
    if (new_node == NULL)
    {
        node_alloc_exc();
    }

    new_node->data = 0;
    new_node->next = NULL;

    return new_node;
}

Node* create_node(int input)
{
    Node* new_node;
    new_node = (Node*)malloc(sizeof(Node));

    if (new_node == NULL)
    {
        node_alloc_exc();
    }

    new_node->data = input;
    new_node->next = NULL;

    return new_node;
}

int node_get_data(Node* node)
{
    return node->data;
}

void node_set_data(Node* node, int value)
{
    node->data = value;
    return;
}

void present_node(Node* node)
{
    printf("%d ", node->data);
    return;
}

void destroy_node(Node** phNode)
{
    if (*phNode == NULL)
    {
        node_dest_exc();
    }

    free(*phNode);
    *phNode = NULL;

    return;
}

void node_alloc_exc(void)
{
    printf("Error: unable to allocate memory for Node. Ending program...\n");
    exit(1);
}

void node_dest_exc(void)
{
    printf("Error: Unable to find Node for deletion. Ending program...\n");
    exit(1);
}
// Function Implementations for Node.
//---------------------------------------------------------------

Leaf* create_leaf_default(void)
{
    Leaf* new_leaf;
    new_leaf = (Leaf*)malloc(sizeof(Leaf));

    if(new_leaf == NULL)
    {
        leaf_alloc_exc();
    }

    new_leaf->data = 0;
    new_leaf->left = NULL;
    new_leaf->right = NULL;

    return new_leaf;
}

Leaf* create_leaf(int input)
{
    Leaf* new_leaf;
    new_leaf = (Leaf*)malloc(sizeof(Leaf));
    
    if (new_leaf == NULL)
    {
        leaf_alloc_exc();
    }

    new_leaf->data = input;
    new_leaf->left = NULL;
    new_leaf->right = NULL;

    return new_leaf;
}

int leaf_get_data(Leaf* leaf)
{
    return leaf->data;
}

void leaf_set_data(Leaf* leaf, int value)
{
    leaf->data = value;
    return;
}

void present_leaf(Leaf* leaf)
{
    printf("%d ", leaf->data);
    return;
}

void destroy_leaf(Leaf** phLeaf)
{
    if  (*phLeaf == NULL)
    {
        leaf_dest_exc();
    }

    free(*phLeaf);
    *phLeaf = NULL;

    return;
}

void leaf_alloc_exc(void)
{
    printf("Error: unable to allocate memory for Leaf. Ending program...\n");
    exit(1);
}

void leaf_dest_exc(void)
{
    printf("Error: unable to find Leaf structure to delete. Ending program...\n");
    exit(1);
}
// Function Implementations for Leaf.
//---------------------------------------------------------------