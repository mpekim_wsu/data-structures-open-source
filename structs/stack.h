#ifndef STACK_H_
#define STACK_H_
#include "linkedlist.h"

struct stack
{
    Node* head;
};

typedef struct stack Stack;

Stack* create_stack_default(void);
Stack* create_stack(int input);

void view_top(Stack* stack);
void push(Stack* stack, int value);

int pop(Stack* stack);

Boolean stack_is_empty(Stack* stack);

void destroy_stack(Stack** phStack);

void stack_alloc_exc(void);
void stack_null_exc(void);
// Function Declarations for Stack.
//---------------------------------------------------------------
#endif // STACK_H_