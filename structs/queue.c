#include "queue.h"

Queue* create_queue_default(void)
{
    Queue* new_queue;
    new_queue = (Queue*)malloc(sizeof(Queue));
    
    if (new_queue == NULL)
    {
        queue_alloc_exc();
    }

    new_queue->head = create_node_default();

    return new_queue;
}

Queue* create_queue(int input)
{
    Queue* new_queue;
    new_queue = (Queue*)malloc(sizeof(Queue));
    
    if (new_queue == NULL)
    {
        queue_alloc_exc();
    }

    new_queue->head = create_node(input);

    return new_queue;
}

void enqueue(Queue* list, int value)
{
    if (list == NULL)
    {
        queue_null_exc();
    }

    if (list->head == NULL)
    {
        list->head = create_node(value);
    }
    // If the Queue exists, but is not empty,
    // add the node to the end of the Queue.
    Node* temp;
    temp = list->head;

    while (temp->next != NULL)
    {
        temp = temp->next;
    }

    temp->next = create_node(value);

    return;
}

void dequeue(Queue* list)
{
    // Remove the node at the start of the Queue.
    if (list == NULL)
    {
        queue_null_exc();
    }
    if (list->head == NULL)
    {
        printf("Queue is empty.\n");
        return;
    }

    Node* temp;
    temp = list->head;

    list->head = temp->next;

    destroy_node(&temp);

    return;
}

void present_queue(Queue* list)
{
    Node* temp;
    temp = list->head;

    if (temp == NULL)
    {
        queue_null_exc();
    }

    while (temp->next != NULL)
    {
        printf("%d ", temp->data);
        temp = temp->next;
    }
    
    printf("%d\n", temp->data);

    return;
}

void find_value(Queue* list, int value)
{
    Node* temp;
    temp = list->head;

    if (temp == NULL)
    {
        queue_null_exc();
    }

    while (temp->next != NULL)
    {
        if (temp->data == value)
        {
            printf("Data found.\n");
            return;
        }
    }

    if (temp->data == value)
    {
        printf("Data found.\n");
        return;
    }

    printf("Data not found within Queue.\n");
    return;

}

Boolean queue_is_empty(Queue* list)
{
    if (list->head == NULL)
    {
        printf("Queue is empty.\n");
        return TRUE;
    }

    printf("Queue is not empty.\n");
    return FALSE;
}

void add_item(Queue* list, int value)
{
    Node* temp;
    temp = list->head;

    if (temp == NULL)
    {
        temp = create_node(value);
        list->head = temp;
        
        return;
    }

    while (temp->next != NULL)
    {
        temp = temp->next;
    }

    temp = create_node(value);
    list->head = temp;
    
    return;
}

void destroy_queue(Queue** phQueue)
{
    if (*phQueue == NULL)
    {
        printf("Error: unable to find Queue for deletion. Ending program...\n");
        exit(1);
    }

    free(*phQueue);
    *phQueue = NULL;

    return;
}

void queue_alloc_exc(void)
{
    printf("Error: unable to allocate data for new Queue. Ending program...\n");
    exit(1);
}

void queue_null_exc(void)
{
    printf("Error: Queue does not exist. Ending program...\n");
    exit(1);
}