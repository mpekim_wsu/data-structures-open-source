#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_
#include "node.h"

struct linkedlist
{
    Node* head;
};

typedef struct linkedlist LinkedList;

LinkedList* create_linkedlist_default(void);
LinkedList* create_linkedlist(int input);

void reverse_list(LinkedList* list);
void present_list(LinkedList* list);
void add_node_end(LinkedList* list, int value);
void remove_node(LinkedList* list, int value);

LinkedList** split_list_half(LinkedList* list);

Boolean list_is_empty(LinkedList* list);

void destroy_linkedlist(LinkedList** phList);

void linkedlist_alloc_exc(void);
void linkedlist_null_exc(void);
void linkedlist_empty_exc(void);
// Decide on whether vertical alignment
// should be based on return type or
// function type.
#endif // LINKEDLIST_H_