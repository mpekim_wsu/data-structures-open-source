#include "linkedlist.h"

LinkedList* create_linkedlist_default(void)
{
    LinkedList* new_list;
    new_list = (LinkedList*)malloc(sizeof(LinkedList));
    
    if (new_list == NULL)
    {
        linkedlist_alloc_exc();
    }

    new_list->head = create_node_default();

    return new_list;
}

LinkedList* create_linkedlist(int input)
{
    LinkedList* new_list;
    new_list = (LinkedList*)malloc(sizeof(LinkedList));
    
    if (new_list == NULL)
    {
        linkedlist_alloc_exc();
    }

    new_list->head = create_node(input);

    return new_list;
}

void reverse_list(LinkedList* list)
{
    Node* temp;
    temp = list->head;
    
    Node* next;
    Node* current;
    Node* previous;

    if (temp == NULL)
    {
        linkedlist_empty_exc();
    }
    // If the list is not empty, then travel
    // to each node and reverse the pointers.
    while (temp->next != NULL)
    {
        // Set each node to point to the previous one.
        temp = temp->next;
    }
    // Do the process one more time for the end of list.
    
    return;
}

void present_list(LinkedList* list)
{
    //printf("Presenting list...\n");
    Node* temp;
    temp = list->head;

    if (temp == NULL)
    {
        linkedlist_null_exc();
    }

    while (temp->next != NULL)
    {
        printf("%d -> ", temp->data);
        temp = temp->next;
    }

    printf("%d\n", temp->data);
    return;
}

void add_node_end(LinkedList* list, int value)
{
    Node* temp;
    temp = list->head;

    if (temp == NULL)
    {
        linkedlist_null_exc();
    }

    while (temp->next != NULL)
    {
        temp = temp->next;
    }

    if (temp->next == NULL)
    {
        temp->next = create_node(value);
    }
    return;
}

void remove_node(LinkedList* list, int value)
{
    //printf("Attempting to remove node...\n");
    // Find the node with the desired value, then delete it.
    Node* temp;
    Node* removed_node;

    temp = list->head;

    if (temp == NULL)
    {
        linkedlist_null_exc();
    }

    if (temp->data == value)
    {
        printf("Data found at start. Removing node...\n");
        removed_node = temp;
        list->head = temp->next;

        destroy_node(&removed_node);
        return;
    }

    // If the list exists, then search for the desired node and delete it.
    while (temp->next != NULL)
    {

        if (temp->next->data == value)
        {
            printf("Data found. Removing node...\n");

            removed_node = temp->next;
            temp->next = temp->next->next;
            
            destroy_node(&removed_node);
            return;
        }
        temp = temp->next;
    }
    
    if (temp->data == value)
    {
        printf("Data found at end. Removing node...\n");

        removed_node = temp;
        temp->next = NULL;

        destroy_node(&removed_node);
    }
    
    return;
}

LinkedList** split_list_half(LinkedList* list)
{
    Node* temp;
    Node* temp_fast;

    temp = list->head;
    temp_fast = list->head;

    while (temp_fast->next != NULL)
    {
        temp = temp->next;
        temp_fast = temp_fast->next->next;
    }
    
}

// Pre-Condition: This function accepts a pointer to a LinkedList
// object.
// Post-Condition: A Boolean value (TRUE or FALSE) will be returned,
// based on whether or not the list is empty.
Boolean list_is_empty(LinkedList* list)
{
    if (list->head == NULL)
    {
        printf("List is empty.\n");
        return TRUE;
    }

    printf("List is not empty.\n");

    return FALSE;
}

// Pre-Condition: This function accepts a "pointer to handle" (AKA double-pointer)
// for a LinkedList object.
// Post-Condition: The LinkedList object will be destroyed, and the memory freed.
void destroy_linkedlist(LinkedList** phList)
{
    if (*phList == NULL)
    {
        printf("Error: unable to find LinkedList for deletion. Ending program...\n");
        exit(1);
    }

    // LinkedList should have all nodes deleted BEFORE deleting the entire list.
    // Research how to do this online.
    
    free(*phList);
    phList = NULL;

    return;
}

void linkedlist_alloc_exc(void)
{
    printf("Error 1: Unable to allocate memory for LinkedList. Ending program...\n");
    exit(1);
}

void linkedlist_null_exc(void)
{
    printf("Error 2: LinkedList does not exist. Ending program...\n");
    exit(2);
}

void linkedlist_empty_exc(void)
{
    printf("Error 3: List is empty. Ending program...\n");
    exit(3);
}