#ifndef BINARY_TREE_H_
#define BINARY_TREE_H_
#include "linkedlist.h"

struct binaryTree
{
    Leaf* root;
};

typedef struct binaryTree BinaryTree;

// Struct for Binary Tree.

BinaryTree* create_binary_tree_default(void);
BinaryTree* create_binary_tree(int data);

void add_leaf_left(BinaryTree* tree, int data);
void add_leaf_right(BinaryTree* tree, int data);

void present_preorder(Leaf* leaf);
void present_postorder(Leaf* leaf);
void present_inorder(Leaf* leaf);

void destroy_binary_tree(BinaryTree** phTree);
void remove_leaf(BinaryTree* tree, int data);

void binary_tree_alloc_exc(void);
void binary_tree_null_exc(void);
// Function Declarations for Binary Tree.
//---------------------------------------------------------------
#endif // BINARY_TREE_H_