#include "binary_tree.h"

BinaryTree* create_binary_tree_default(void)
{
    BinaryTree* new_tree;
    new_tree = (BinaryTree*)malloc(sizeof(BinaryTree));
    
    if (new_tree == NULL)
    {
        binary_tree_alloc_exc();
    }

    new_tree->root = create_leaf_default();

    return new_tree;
}

BinaryTree* create_binary_tree(int data)
{
    BinaryTree* new_tree;
    new_tree = (BinaryTree*)malloc(sizeof(BinaryTree));
    
    if (new_tree == NULL)
    {
        binary_tree_alloc_exc();
    }

    new_tree->root = create_leaf(data);

    return new_tree;
}

void add_leaf_left(BinaryTree* tree, int data)
{
    Leaf* temp;
    temp = tree->root;

    if (temp == NULL)
    {
        binary_tree_null_exc();
    }

    while (temp->left != NULL)
    {
        temp = temp->left;
    }

    if (temp->left == NULL)
    {
        temp->left = create_leaf(data);
        //printf("Left leaf created.\n");
    }

    return;
}

void add_leaf_right(BinaryTree* tree, int data)
{
    Leaf* temp;
    temp = tree->root;
    
    if (temp == NULL)
    {
        binary_tree_null_exc();
    }

    while (temp->right != NULL)
    {
        temp = temp->right;
    }

    if (temp->right == NULL)
    {
        temp->right = create_leaf(data);
    }

    return;
}

void present_preorder(Leaf* leaf)
{
    Leaf* temp;
    temp = leaf;

    if (temp == NULL)
    {
        return;
    }

    // Check to see if there is only one node in the tree.

    present_leaf(temp);
    present_preorder(temp->left);
    present_preorder(temp->right);

    return;
}

void present_postorder(Leaf* leaf)
{
    Leaf* temp;
    temp = leaf;

    if (temp == NULL)
    {
        return;
    }

    present_postorder(temp->left);
    present_postorder(temp->right);
    present_leaf(temp);

    return;
}

void present_inorder(Leaf* leaf)
{
    Leaf* temp;
    temp = leaf;

    if (temp == NULL)
    {
        return;
    }

    present_inorder(temp->left);
    present_leaf(temp);
    present_inorder(temp->right);

    return;
}

void destroy_binary_tree(BinaryTree** phTree)
{
    if (*phTree == NULL)
    {
        binary_tree_null_exc();
    }

    Leaf* temp;
    temp = *phTree->root;

    free(*phTree);
    *phTree = NULL;
    // This only frees the tree. Be sure to visit and delete the leaves as well.

    return;
}

void remove_leaf(BinaryTree* tree, int data)
{
    // Search for the leaf by its data.
    // When the leaf is found, destroy the leaf.
    Leaf* temp;

    temp = tree->root;

    if (temp == NULL)
    {
        binary_tree_null_exc();
    }

    if (temp->data == data)
    {
        printf("Found data. Removing leaf...");
        // Destroy the leaf. If needed, re-structure the tree.
    }

    // If the leaf is not found, then traverse the tree.
    while (temp->left != NULL)
    {
        // This may require using a leaf instead of the tree as an arugment.
        // NOTE: re-make this function so that it involves recursion.
    }

    return;
}

void binary_tree_alloc_exc(void)
{
    printf("Error: unable to allocate data for new Binary Tree. Ending program...\n");
    exit(1);
}

void binary_tree_null_exc(void)
{
    printf("Error: Binary Tree does not exist. Ending program...\n");
    exit(1);
}
// Function Implementations for Binary Tree.
//---------------------------------------------------------------