#ifndef QUEUE_H_
#define QUEUE_H_
#include "linkedlist.h"

struct queue
{
    Node* head;
};

typedef struct queue Queue;
// Typedef and struct for Queue.

Queue* create_queue_default(void);
Queue* create_queue(int input);

void enqueue(Queue* list, int value);
void dequeue(Queue* list);

void present_queue(Queue* list);
void find_value(Queue* list, int value);

Boolean queue_is_empty(Queue* list);

void add_item(Queue* list, int value);

void destroy_queue(Queue** phQueue);

void queue_alloc_exc(void);
void queue_null_exc(void);
// Function Declarations for Queue.
//---------------------------------------------------------------
#endif //QUEUE_H_