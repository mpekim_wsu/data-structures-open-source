#ifndef NODE_H_
#define NODE_H_
#include <stdio.h>
#include <stdlib.h>

typedef enum boolean Boolean;

typedef struct node Node;
typedef struct leaf Leaf;

struct node
{
    int data;
    Node* next;
};

struct leaf
{
    int data;
    Leaf* left;
    Leaf* right;
};

enum boolean
{
    FALSE,
    TRUE
};

// Typedef and struct for Node/Leaf.
// Comment for next push.
//------------------------------------

Node* create_node_default(void);
Node* create_node(int input);

void destroy_node(Node** phNode);

int node_get_data(Node* node);
void node_set_data(Node* node, int value);
void present_node(Node* node);

void node_alloc_exc(void);
void node_dest_exc(void);
// Function Declarations for Node.

Leaf* create_leaf_default(void);
Leaf* create_leaf(int input);

int leaf_get_data(Leaf* leaf);
void leaf_set_data(Leaf* leaf, int value);

void present_leaf(Leaf* leaf);

void destroy_leaf(Leaf** phLeaf);

void leaf_alloc_exc(void);
void leaf_dest_exc(void);
// Function Declarations for Leaf.
//---------------------------------------------------------------
#endif // NODE_H_