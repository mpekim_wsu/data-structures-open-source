#include "node.h"
#include "stack.h"

Stack* create_stack_default(void)
{
    Stack* new_stack;
    new_stack = (Stack*)malloc(sizeof(Stack));
    
    if (new_stack == NULL)
    {
        stack_alloc_exc();
    }

    new_stack->head = create_node_default();

    return new_stack;
}

Stack* create_stack(int input)
{
    Stack* new_stack;
    new_stack = (Stack*)malloc(sizeof(Stack));

    if (new_stack == NULL)
    {
        stack_alloc_exc();
    }

    new_stack->head = create_node(input);

    return new_stack;
}

// Pre-Condition: This function accepts a Stack argument.
// Post-Condition: The top value of the stack is viewed.
void view_top(Stack* stack)
{
    Node* temp;
    temp = stack->head;

    printf("%d\n", temp->data);
    return;
}

void push(Stack* stack, int value)
{
    Node* temp;
    temp = stack->head;

    if (temp == NULL)
    {
        stack_null_exc();
    }

    Node* new_node;
    
    new_node = create_node(value);
    new_node->next = stack->head;

    stack->head = new_node;
    
    return;
}

int pop(Stack* stack)
{
    Node* temp;
    temp = stack->head;

    int data;

    if (temp == NULL)
    {
        stack_null_exc();
    }

    if (temp->next == NULL)
    {
        printf("Popping last value...\n");
        data = temp->data;
        stack->head = NULL;
        destroy_node(&temp);
        return data;
    }

    data = temp->data;
    stack->head = temp->next;

    destroy_node(&temp);

    return data;
}

Boolean stack_is_empty(Stack* stack)
{
    if (stack->head == NULL)
    {
        return TRUE;
    }
    
    return FALSE;
}

void destroy_stack(Stack** phStack)
{
    if (*phStack == NULL)
    {
        printf("Error: Unable to find Stack object to destroy. Ending program...\n");
        exit(1);
    }

    free(*phStack);
    *phStack = NULL;

    printf("Stack successfully destroyed.\n");
    
    return;
}

void stack_alloc_exc(void)
{
    printf("Unable to allocate memory for Stack. Ending program...\n");
    exit(1);
}

void stack_null_exc(void)
{
    printf("Error: Stack does not exist. Ending program...\n");
    exit(1);
}

// Function Implementations for Stack.
//---------------------------------------------------------------