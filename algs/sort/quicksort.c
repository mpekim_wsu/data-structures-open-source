int partition(int arr[], int low, int high)
{
    int pivot;
    int i;

    pivot = arr[high];

    i = low - 1;

    for (int j = low; j <= high; j++)
    {
        if (arr[j] < pivot)
        {
            i++;
            swap(arr[i], arr[j]);
        }
    }
    swap(arr[i + 1], arr[high]);

    return (i + 1);
}

void quicksort(int arr[], int low, int high)
{
    // Take elements and determine pivot (usually the final element).
    // Compare each element, swapping as needed.
    // Elements smaller than pivot to left, larger to right.
    // Once the pivot is in place, repeat on smaller sub-sections.
    if (low < high)
    {
        int pivot = partition(arr, low, high);

        quicksort(arr, low, pivot - 1);
        quicksort(arr, pivot + 1, high);
    }
    
    return;
}

// Based off of website:
// https://www.geeksforgeeks.org/quick-sort/