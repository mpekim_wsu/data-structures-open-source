#include "insertion_sort.h"

void insertion_sort(int* list, int size)
{
    int i;
    int key;
    int j;

    for (i = 1; i < size; i++)
    {
        key = list[i];
        j = i - 1;

        while (j >= 0 && list[j] > key)
        {
            list[j + 1] = list[j];
            j = j - 1;
        }
        
        list[j + 1] = key;
    }
    
    return;
}

// Code found at Geeks for Geeks:
// https://www.geeksforgeeks.org/insertion-sort/