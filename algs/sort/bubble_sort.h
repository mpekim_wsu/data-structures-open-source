#ifndef BUBBLE_SORT_H_
#define BUBBLE_SORT_H_
#include "../basic_func.h"

void bubble_sort(int* arr, int size);

#endif // BUBBLE_SORT_H_