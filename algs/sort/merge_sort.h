#ifndef MERGESORT_H_
#define MERGESORT_H_
#include "../basic_func.h";

void merge_sort(int* list, int left, int right, int middle);

void merge(int* list, int left, int middle, int right);

#endif //MERGESORT_H_