#ifndef INSERTION_SORT_H_
#define INSERTION_SORT_H_
#include "../basic_func.h"

void insertion_sort(int* list, int size);

#endif // INSERTION_SORT_H_