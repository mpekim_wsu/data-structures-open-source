#ifndef QUICKSORT_H_
#define QUICKSORT_H_

#include "../basic_func.h";

int partition(int arr[], int low, int high);

void quicksort(int* list, int low, int high);

#endif // QUICKSORT_H_