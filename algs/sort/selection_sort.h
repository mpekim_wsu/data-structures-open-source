#ifndef SELECTION_SORT_H_
#define SELECTION_SORT_H_

#include "../basic_func.h"
void selection_sort(int* arr, int size);

#endif // SELECTION_SORT_H_