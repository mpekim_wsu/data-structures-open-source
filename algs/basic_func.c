#include "basic_func.h"
// Pre-Condition: This function accepts two integer pointers,
// for the values that will be swapped.
// Post-Condition: The variables will have their values swapped.
void swap(int* a, int* b)
{
    //printf("ENTER SWAP\n");
    int temp;

    temp = *a;
    *a = *b;
    *b = temp;
    
    return;
}

void present(int* list, int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("%d ", list[i]);
    }
    
    printf("\n");
}

// Pre-Condition: This function accepts no arguments.
// Post-Condition: The keyboard buffer will be cleared.
void clear_keyboard_bufffer(void)
{
    char c;

    do{
        scanf("%c", &c);
    } while (c != '\n');
    
    return;
}