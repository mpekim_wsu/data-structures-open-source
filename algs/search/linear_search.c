#include "linear_search.h";

int linear_search(int list[], int item, int size)
{
    for (int i = 0; i < size; i++)
    {
        if (list[i] == item)
        {
            return list[i];
        }
    }
    return -1;
}