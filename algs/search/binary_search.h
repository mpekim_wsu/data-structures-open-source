#ifndef BINARY_SEARCH_H_
#define BINARY_SEARCH_H_
#include "../basic_func.h"

int binary_search(int list[], int item, int size);

#endif // BINARY_SEARCH_H_