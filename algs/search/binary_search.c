#include "binary_search.h"

int binary_search(int list[], int item, int size)
{
    // Check to see if the desired element is found.
    // This item will be checked by the median
    // (middle value). If so, return it.

    // If it is not found, then divide the list in two.
    // Take the respective half (higher/lower) and
    // search there.
    int low = 0;
    int high = size;
    int median = (high / 2);

    while (low < high)
    {
        if (list[median] == item)
        {
            return item;
        }
        else
        {
            if (list[median] > item)
            {
                high = median - 1;
                median = (high - low) / 2;
            }
            else
            {
                low = median + 1;
                median = (high - low) / 2 + low;
            }
        }
    }
    return -1;
}