#ifndef LINEAR_SEARCH_H_
#define LINEAR_SEARCH_H_
#include "../basic_func.h"

int linear_search(int list[], int item, int size);

#endif //LINEAR_SEARCH_H_s