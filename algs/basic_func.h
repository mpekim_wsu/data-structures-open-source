#ifndef BASIC_FUNC_H_
#define BASIC_FUNC_H_
#include <stdio.h>
#include <stdlib.h>

void swap(int* a, int* b);
void present(int* list, int size);

void clear_keyboard_buffer(void);
#endif // BASIC_FUNC_H_